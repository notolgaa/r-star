#ifndef SEARCH_H
#define SEARCH_H
#include "list.h"
#include "map.h"
#include "searchresult.h"
#include "logger.h"
#include "environmentoptions.h"
#include <unordered_map>
#include <map>
class Search
{
    public:
        Search();
        virtual ~Search(void);

        SearchResult startSearch(Logger *log, const Map &Map, const EnvironmentOptions &options);
        //void printResultToConsole(Logger *log, SearchResult &sr);
        Node findMin(int size);
        float MoveCost(int start_i, int start_j, int fin_i, int fin_j, const EnvironmentOptions &options);

    protected:
        virtual void addOpen(Node newNode) = 0; //каждый поиск по своему добавляет вершины в список OPEN
        virtual float computeHFromCellToCell(int start_i, int start_j, int fin_i, int fin_j, const EnvironmentOptions &options) = 0; //для Дейкстры и BFS этот метод всегда возвращает ноль
        virtual std::list<Node> findSuccessors(Node curNode, const Map &map, const EnvironmentOptions &options);//метод, который ищет соседей текущей вершины, удовлетворяющие параметрам поиска
        virtual void makePrimaryPath(Node curNode);//строит путь по ссылкам на родителя
        virtual void makeSecondaryPath(const Map &map, Node curNode);//разбивает найденный путь на секции, содержащие только прямые участки
        virtual void printResultToConsole(Logger *log,const Map &map, SearchResult &sr);
        virtual Node resetParent(Node current, Node parent, const Map &map, const EnvironmentOptions &options) { return current;}//меняет родителя, нужен для алгоритма Theta*
        bool stopCriterion();
        SearchResult    sresult, sresultlocal; //результат поиска
        NodeList         lppath, hppath, openless; //списки OPEN, CLOSE и путь
        Node lastnode;
        std::unordered_map<int,Node> close;
        NodeList *open;
        int     openSize;
        int     sizelimit; //ограничение на размер OPEN
        float   hweight; //вес эвристики
        int     breakingties; //критерий выбора очередной вершины из OPEN, когда F-значений равны
};

#endif // SEARCH_H
