#include "rstar.h"
#include <stdlib.h>
#include <math.h>
#include "search.h"
#include "astar.h"
#include <iostream>
#include "gl_const.h"
#include "searchresult.h"
#ifdef __linux__
    #include <sys/time.h>
#else
    #include <windows.h>
#endif


Rstar::Rstar(float w, int BT, int SL, int i)
{
    hweight = w;
    breakingties = BT;
    sizelimit = SL;
    //open=new NodeList[i];
}

Rstar::~Rstar()
{
}


SearchResult Rstar::findLocalPath(Node &curNode, NodeList &successorsDist, const EnvironmentOptions &options, NodeList open, const Map &map )
{
    //необходимо найти путь к каждой вершине списка соседних "на расстоянии", кратном distance
    //поэтому делаем цикл, чтобы просчитать пути к ним (локальные)

    std::list<Node>::iterator it=successorsDist.begin();
    while(it!=successorsDist.end())
    {
        while(!stopCriterion())
        {
            #ifdef __linux__
                timeval begin, end;
                gettimeofday(&begin, NULL);
            #else
                LARGE_INTEGER begin, end, freq;
                QueryPerformanceCounter(&begin);
                QueryPerformanceFrequency(&freq);
            #endif

    for(int i=0; i<map.height; i++)

        //считаем Н от текущей вершины до рассмативаемой из списка successorsDist
    curNode.H = computeHFromCellToCell(curNode.i,curNode.j, it->i, it->j, options);
    curNode.F = hweight * curNode.H;
    //curNode.parent = 0;

    //просчитав Н, ищем локальный путь от текущей вершины до соседней
    //(алгоритм тот же как и в Search::startSearch только произошла замена в Open/Close и целевой вершине
    addOpen(curNode);
    int closeSize=0;
    bool localpathfound=false;
    while(!stopCriterion())
    {
        curNode = findMin(map.height);
        close.insert({curNode.i*map.width+curNode.j,curNode});
        closeSize++;
        open[curNode.i].List.pop_front();
        openSize--;
        if(curNode.i==it->i && curNode.j==it->j)
        {

            localpathfound=true;
            break;
        }
        std::list<Node> successors=findSuccessors(curNode,map,options);
        std::list<Node>::iterator itLocal=successors.begin();
        while(itLocal!=successors.end())
        {
            itLocal->parent = &(close.find(curNode.i*map.width+curNode.j)->second);
            itLocal->H = computeHFromCellToCell(itLocal->i,itLocal->j,it->i,it->j,options);
            *itLocal=resetParent(*itLocal, *itLocal->parent, map, options);
            itLocal->F = itLocal->g + hweight * itLocal->H;
            addOpen(*itLocal);
            itLocal++;
        }
        log->writeToLogOpenClose(open,close,map.height); //Логгер сам определит писать ему в лог или нет.

    }

    sresultlocal.pathfound = false;
    sresultlocal.nodescreated = closeSize + openSize;
    sresultlocal.numberofsteps = closeSize;
    if(localpathfound)
    {
        sresultlocal.pathfound = true;
        sresultlocal.pathlength = curNode.g;
        //путь восстанолвенный по обратным указателям (для всех алгоритмов)
        makePrimaryPath(curNode);
    }
    //Т.к. восстановление пути по обратным указателям - неотъемлемая часть алгоритмов, время останавливаем только сейчас!
    #ifdef __linux__
        gettimeofday(&end, NULL);
        sresult.time = (end.tv_sec - begin.tv_sec) + static_cast<double>(end.tv_usec - begin.tv_usec) / 1000000;
    #else
            QueryPerformanceCounter(&end);
            sresultlocal.time = static_cast<double>(end.QuadPart - begin.QuadPart)/freq.QuadPart;
    #endif
    //перестроенный путь (hplevel, либо lplevel,в зависимости от алгоритма)
    if(localpathfound)
            makeSecondaryPath(map, curNode);


        }
    }


}

std::list<Node> Rstar::findSuccessors(Node curNode, const Map &map, const EnvironmentOptions &options, int distance)
{
    Node newNode;
    std::list<Node> successorsDist;
    for(int i = -1; i <= +1; i++)
    {
        for(int j = -1; j <= +1; j++)
        {
            if((i != 0 || j != 0) && map.CellOnGrid(curNode.i+i*distance,curNode.j+j*distance)
                    && (map.CellIsTraversable(curNode.i+i*distance,curNode.j+j*distance)))
            {
                if(options.allowdiagonal == CN_SP_AD_FALSE)
                    if(i != 0 && j != 0)
                        continue;
                if(options.allowsqueeze == CN_SP_AS_FALSE)
                {
                    if(i != 0 && j != 0)
                        if(map.CellIsObstacle(curNode.i,curNode.j+j*distance)
                                && map.CellIsObstacle(curNode.i+i*distance,curNode.j))
                            continue;
                }
                if(close.find((curNode.i+i*distance)*map.width+curNode.j+j*distance)==close.end())
                {
                    newNode.i = curNode.i+i*distance;
                    newNode.j = curNode.j+j*distance;
                    newNode.g = curNode.g + MoveCost(curNode.i,curNode.j,curNode.i+i*distance,curNode.j+j*distance,options);
                    successorsDist.push_front(newNode);
                }
            }
        }
    }
    return successorsDist;
}

