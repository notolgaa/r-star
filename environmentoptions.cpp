#include <string>
#include <algorithm>
#include <sstream>
#include <iostream>
#include "environmentoptions.h"
#include "gl_const.h"
#include "tinystr.h"
#include "tinyxml.h"
#include "gl_const.h"

EnvironmentOptions::EnvironmentOptions()
{
    metrictype = -1;
    searchtype = -1;
    linecost   = -1;
    diagonalcost = -1;
    allowdiagonal = CN_SP_AD_FALSE;
    allowsqueeze = CN_SP_AS_FALSE;
    hweight = -1;
    breakingties = CN_SP_BT_GMIN;
}

EnvironmentOptions::EnvironmentOptions(int MT, int ST, float LC, float DC, bool AD, bool AS, float HW, int BT)
{
    metrictype = MT;
    searchtype = ST;
    linecost = LC;
    diagonalcost = DC;
    allowdiagonal = AD;
    allowsqueeze = AS;
    hweight = HW;
    breakingties = BT;
}

bool EnvironmentOptions::getOptions(const char* FileName)
{
    TiXmlElement *root = 0, *algorithm = 0, *element = 0;
    TiXmlNode *algTag=0;

    std::string value; //имя тега
    std::stringstream stream; //содердимое (текст) тега

    bool hasMT=false, hasST=false, hasLC=false, hasDC=false, hasAD=false, hasHW=false, hasBT=false;

    TiXmlDocument file(FileName);

    // Load XML File
    if (!file.LoadFile())
    {
        std::cout << "Error opening XML file!" << std::endl;
        return false;
    }

    std::cout <<""<< std::endl;
    std::cout <<"Algorithm characteristics:"<< std::endl << std::endl;

    root = file.FirstChildElement(CNS_TAG_ROOT);
    if (!root)
    {
        std::cout << "Error! No '"<< CNS_TAG_ROOT <<"' tag found in XML file!" << std::endl;
        return false;
    }

    algorithm = root -> FirstChildElement(CNS_TAG_ALG);
    if (!algorithm)
    {
        std::cout << "Error! No '"<< CNS_TAG_ALG <<"' tag found in XML file!" << std::endl;
        return false;
    }

    algTag=algorithm->FirstChild();//tag "algorithm"

    while(algTag)
    {
        std::string ss;
        ss.clear();
        element=algTag->ToElement();
        value=algTag->Value();
        stream.str("");
        stream.clear();
        stream << element->GetText();

//LineCost
            if(value==CNS_TAG_LC)
            {
                if (hasLC)
                {
                   std::cout << "Warning! Duplicate '"<< CNS_TAG_LC <<"' encountered." << std::endl;
                   std::cout << "Only first value of '"<< CNS_TAG_LC <<"' =" << linecost <<"will be used." << std::endl;
                }

                else
                {
                    if (!(stream >> linecost && linecost>0)&& hasLC!=true)
                    {
                         std::cout << "Warning! Invalid value of '" << CNS_TAG_LC <<"' tag encountered." << std::endl;
                    }
                    else
                    {
                      hasLC=true;    std::cout << "LineCost   =   "<< linecost << std::endl;
                    }
                }
            }

//DiagonalCost
             if(value==CNS_TAG_DC)
             {
                 if (hasDC)
                 {
                    std::cout << "Warning! Duplicate '"<< CNS_TAG_DC <<"' encountered." << std::endl;
                    std::cout << "Only first value of '"<< CNS_TAG_DC <<"' =" << diagonalcost <<"will be used." << std::endl;
                 }

                 else
                 {
                    if (!(stream >> diagonalcost && diagonalcost>0)&& hasDC!=true)
                    {
                        std::cout << "Warning! Invalid value of '" << CNS_TAG_DC <<"' tag encountered." << std::endl;
                    }
                    else
                    {
                    hasDC=true;    std::cout << "DiagonalCost   =   "<< diagonalcost << std::endl;
                    }
                }
              }

//Hweight
             if(value==CNS_TAG_HW)
             {
                 if (hasHW)
                 {
                    std::cout << "Warning! Duplicate '"<< CNS_TAG_HW <<"' encountered." << std::endl;
                    std::cout << "Only first value of '"<< CNS_TAG_HW <<"' =" << hweight <<"will be used." << std::endl;
                 }

                 else
                 {
                    if (!(stream >> hweight && hweight>0)&& hasHW!=true)
                    {
                        std::cout << "Warning! Invalid value of '" << CNS_TAG_HW <<"' tag encountered." << std::endl;
                    }
                    else
                    {
                    hasHW=true;    std::cout << "HWeight   =   "<< hweight << std::endl;
                    }
                }
              }


//Breakingties
             if(value==CNS_TAG_BT)
             {
                 if (hasBT)
                 {
                    std::cout << "Warning! Duplicate '"<< CNS_TAG_BT <<"' encountered." << std::endl;
                    std::cout << "Only first value of '"<< CNS_TAG_BT <<"' =" << breakingties <<"will be used." << std::endl;
                 }

                 else
                 {

                     if (hasBT!=true && strcmp(ss.c_str(), CNS_SP_BT_GMAX) == 0)
                     {
                         breakingties = CN_SP_BT_GMAX;
                         hasBT = true;
                         std::cout << "Breakingties   =   "<< breakingties << std::endl;
                     }

                     if (hasBT!=true && strcmp(ss.c_str(), CNS_SP_BT_GMIN) == 0)
                     {
                         breakingties = CN_SP_BT_GMIN;
                         hasBT = true;
                         std::cout << "Breakingties   =   "<< breakingties << std::endl;
                     }

                     if (hasBT != true)
                     {
                         std::cout << "Warning! Invalid value of '" << CNS_TAG_BT <<"' tag encountered." << std::endl;
                     }

                }
              }

//AllowDiagonal
              if(value==CNS_TAG_AD)
              {
                 if (!(stream >> allowdiagonal)&& hasAD!=true)
                 {
                    std::cout << "Warning! Invalid value of '" << CNS_TAG_AD <<"' tag encountered." << std::endl;
                 }
                    else
                 {
                    hasAD=true;    std::cout << "AllowDiagonal   =   "<< allowdiagonal << std::endl;
                 }
              }

stream >> ss;
//Metrictype

              if(value==CNS_TAG_MT)
              {
                  if (hasMT)
                  {
                     std::cout << "Warning! Duplicate '"<< CNS_TAG_MT <<"' encountered." << std::endl;
                     std::cout << "Only first value of '"<< CNS_TAG_MT <<"' =" << metrictype <<"will be used." << std::endl;
                  }

                  else
                  {
                    if (hasMT!=true && strcmp(ss.c_str(), CNS_SP_MT_DIAG) == 0)
                     {
                         hasMT=true; metrictype = CN_SP_MT_DIAG; std::cout << "Metrictype = "<< CNS_SP_MT_DIAG << std::endl;
                     }

                     if (hasMT!=true && strcmp(ss.c_str(), CNS_SP_MT_MANH) == 0)
                     {
                         hasMT=true;  metrictype = CN_SP_MT_MANH; std::cout << "Metrictype = "<< CNS_SP_MT_MANH << std::endl;
                     }

                     if (hasMT!=true && strcmp(ss.c_str(), CNS_SP_MT_EUCL) == 0)
                     {
                         hasMT=true;  metrictype = CN_SP_MT_EUCL; std::cout << "Metrictype = "<< CNS_SP_MT_EUCL << std::endl;
                     }

                     if (hasMT!=true && strcmp(ss.c_str(), CNS_SP_MT_CHEB) == 0)
                     {
                         hasMT=true;  metrictype = CN_SP_MT_CHEB; std::cout << "Metrictype = "<< CNS_SP_MT_CHEB << std::endl;
                     }
                  }

                }

//Searchtype

                if(value==CNS_TAG_ST)
                {
                    if (hasST)
                    {
                       std::cout << "Warning! Duplicate '"<< CNS_TAG_ST <<"' encountered." << std::endl;
                       std::cout << "Only first value of '"<< CNS_TAG_ST <<"' =" << searchtype <<"will be used." << std::endl;
                    }

                    else
                    {

                     if (hasST!=true && strcmp(ss.c_str(), CNS_SP_ST_BFS) == 0)
                     {
                         hasST=true; searchtype = CN_SP_ST_BFS; std::cout << "Searchtype = "<< CNS_SP_ST_BFS << std::endl;
                     }

                     if (hasST!=true && strcmp(ss.c_str(), CNS_SP_ST_JP_SEARCH) == 0)
                     {
                         hasST=true; searchtype = CN_SP_ST_JP_SEARCH; std::cout << "Searchtype = "<< CNS_SP_ST_JP_SEARCH << std::endl;
                     }

                     if (hasST!=true && strcmp(ss.c_str(), CNS_SP_ST_DIJK) == 0)
                     {
                         hasST=true; searchtype = CN_SP_ST_DIJK; std::cout << "Searchtype = "<< CNS_SP_ST_DIJK << std::endl;
                     }

                     if (hasST!=true && strcmp(ss.c_str(),CNS_SP_ST_ASTAR)== 0)
                     {
                         hasST=true; searchtype = CN_SP_ST_ASTAR; std::cout << "Searchtype = "<< CNS_SP_ST_ASTAR << std::endl;
                     }

                     if (hasST!=true && strcmp(ss.c_str(), CNS_SP_ST_TH) == 0)
                     {
                         hasST=true; searchtype = CN_SP_ST_TH; std::cout << "Searchtype = "<< CNS_SP_ST_TH << std::endl;
                     }
                   }

                }

            algTag=algorithm->IterateChildren(algTag);
    }
    if(!hasMT)
    {
        std::cout << "Error! Tag '"<< CNS_TAG_MT<<"' wasn't found in the file!"<< std::endl;
    }

    if(!hasST)
    {
        std::cout << "Error! Tag '"<< CNS_TAG_ST <<"' wasn't found in the file!"<< std::endl;
    }

    if(!hasLC)
    {
        std::cout << "Error! Tag '"<< CNS_TAG_LC <<"' wasn't found in the file!"<< std::endl;
    }

    if(!hasDC)
    {
        std::cout << "Error! Tag '"<< CNS_TAG_DC <<"' wasn't found in the file!"<< std::endl;
    }

    return true;
}
