#ifndef ENVIRONMENTOPTIONS_H
#define ENVIRONMENTOPTIONS_H

class EnvironmentOptions
{
public:
    EnvironmentOptions(int MT, int ST, float LC, float DC, bool AD, bool AS, float HW, int BT);
    EnvironmentOptions();
    int     metrictype; //тип метрики для подсчета эвристики
    int     searchtype;
    float   linecost; //стоимость перемещения по горизонтали и вертикали
    float   diagonalcost; //стоимость перемещаения по диагонали
    bool    allowdiagonal; //опция разрешающая/запрещающая перемещения по диагонали
    bool    allowsqueeze; //опция, позволяющая прохождение через узкий коридор
    float   hweight;
    int     breakingties;

    bool getOptions(const char *FileName);

};

#endif // ENVIRONMENTOPTIONS_H
