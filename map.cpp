#include <string>
#include <algorithm>
#include <sstream>
#include <iostream>
#include "map.h"
#include "tinystr.h"
#include "tinyxml.h"
#include "gl_const.h"

Map::Map()
{
    height = -1;
    width = -1;
    start_i = -1;
    start_j = -1;
    goal_i = -1;
    goal_j = -1;
    Grid = NULL;
}

Map::~Map()
{
    if (Grid)
    {
        for (int i = 0; i < height; i++)    delete[] Grid[i];
        delete[] Grid;
    }
}

bool Map::CellIsObstacle(int i, int j) const
{
    return (Grid[i][j] == CN_GC_OBS);
}

bool Map::CellOnGrid(int i, int j) const
{
    return (i < height && i >= 0 && j < width && j >= 0);
}

bool Map::CellIsTraversable(int i, int j) const
{
    return (Grid[i][j] == CN_GC_NOOBS);
}


bool Map::getMap(const char* FileName)
{
    const char *row = 0;
    int rowiter = 0, grid_i = 0, grid_j = 0;

    TiXmlElement *root = 0, *map = 0, *element = 0;
    TiXmlNode *mapTag=0, *gridrow = 0;

    std::string value; //имя тега
    std::stringstream stream; //содердимое (текст) тега

    bool hasGridMem=false, hasGrid=false, hasHeight=false, hasWidth=false, hasSTX=false, hasSTY=false, hasFINX=false, hasFINY=false;

    TiXmlDocument file(FileName);

    // Load XML File
    if (!file.LoadFile())
    {
        std::cout<<std::endl;
        std::cout << "Error opening XML file!" << std::endl;
        return false;
    }
    else std::cout<<std::endl; std::cout<<"The file '"<<FileName<<"' was successfully loaded."<<std::endl<<std::endl;

    std::cout <<"Map characteristics:"<< std::endl << std::endl;

    root = file.FirstChildElement(CNS_TAG_ROOT);
    if (!root)
    {
        std::cout << "Error! No '"<< CNS_TAG_ROOT <<"' tag found in XML file!" << std::endl;
        return false;
    }


    map = root -> FirstChildElement(CNS_TAG_MAP);
    if (!map)
    {
        std::cout << "Error! No '"<< CNS_TAG_MAP <<"' tag found in XML file!" << std::endl;
        return false;
    }

    mapTag=map->FirstChild();//tag "map"
    while(mapTag)
    {
            element=mapTag->ToElement();
            value=mapTag->Value();
            stream.str("");
            stream.clear();
            stream << element->GetText();

 //Grid
            if(!hasGridMem && hasHeight && hasWidth)
            {
                    Grid=new int*[height];
                    for(int i=0; i<height;i++)
                            Grid[i]=new int[width];
                    hasGridMem=true;
            }

 //Height
            if(value==CNS_TAG_HEIGHT)
            {
                if (hasHeight)
                {
                   std::cout << "Warning! Duplicate '"<< CNS_TAG_HEIGHT <<"' encountered." << std::endl;
                   std::cout << "Only first value of '"<< CNS_TAG_HEIGHT <<"' =" << height <<"will be used." << std::endl;
                }
                else
                {
                        if (!((stream >> height) && (height > 0)))
                        {
                             std::cout << "Warning! Invalid value of '"<< CNS_TAG_HEIGHT <<"' tag encountered." << std::endl;
                         }
                        else
                        {
                             hasHeight=true; std::cout << "Height    =   "<< height << std::endl;
                        }
                }
            }


//Width
            if(value==CNS_TAG_WIDTH)
            {

                if (hasWidth)
                {
                   std::cout << "Warning! Duplicate '"<< CNS_TAG_WIDTH <<"' encountered." << std::endl;
                   std::cout << "Only first value of '"<< CNS_TAG_WIDTH <<"' =" << width <<"will be used." << std::endl;
                }
                else
                {
                    if (!((stream >> width) && (width > 0)))
                    {
                        std::cout << "Warning! Invalid value of '"<< CNS_TAG_WIDTH <<"' tag encountered." << std::endl;
                    }
                    else
                    {
                        hasWidth=true;  std::cout << "Width     =   "<< width << std::endl;
                    }
               }
             }


//Start-x
            else if(value==CNS_TAG_STX)
            {
                if (!hasWidth) //Тег "старт-х" встретился раньше чем "ширина" - ошибка!
                {
                    std::cout << "Error! '" << CNS_TAG_STX <<"' tag encountered before '" << CNS_TAG_WIDTH << "' tag." << std::endl;
                    return false;
                }

                if (hasSTX) //Дубль. Старт-ИКС уже был.
                {
                   std::cout << "Warning! Duplicate '"<< CNS_TAG_STX <<"' encountered." << std::endl;
                   std::cout << "Only first value of '"<< CNS_TAG_STX <<"' =" << start_j <<"will be used." << std::endl;
                }
                else //Старт-ИКС еще не встречался (или встречался, но корявый), в общем - надо пытаться инициализировать
                {
                    if (!(stream >> start_j && start_j>=0 && start_j<width))
                    {
                        std::cout << "Warning! Invalid value of '" << CNS_TAG_STX <<"' tag encountered (or could not convert to integer)" << std::endl;
                        std::cout << "Value of '"<< CNS_TAG_STX <<"' tag should be an integer AND >=0 AND < '" << CNS_TAG_WIDTH << "' value, which is " << width << std::endl;
                        std::cout << "Continue reading XML and hope correct value of '"<< CNS_TAG_STX <<"' tag will be encountered later..." << std::endl;
                    }
                    else
                        hasSTX=true;
                }
            }
            //Закончили со Cтарт-Икс


            //5. Старт-Игрек
            else if(value==CNS_TAG_STY)
            {
                if (!hasHeight) //Тег "старт-y" встретился раньше чем "высота" - ошибка!
                {
                    std::cout << "Error! '" << CNS_TAG_STY <<"' tag encountered before '" << CNS_TAG_HEIGHT << "' tag." << std::endl;
                    return false;
                }

                if (hasSTY) //Дубль. Старт-ИКС уже был.
                {
                   std::cout << "Warning! Duplicate '"<< CNS_TAG_STY <<"' encountered." << std::endl;
                   std::cout << "Only first value of '"<< CNS_TAG_STY <<"' =" << start_i <<"will be used." << std::endl;
                }
                else //Старт-Игрек еще не встречался (или встречался, но корявый), в общем - надо пытаться инициализировать
                {
                    if (!(stream >> start_i && start_i>=0 && start_i<height))
                    {
                        std::cout << "Warning! Invalid value of '" << CNS_TAG_STY <<"' tag encountered (or could not convert to integer)" << std::endl;
                        std::cout << "Value of '"<< CNS_TAG_STY <<"' tag should be an integer AND >=0 AND < '" << CNS_TAG_HEIGHT << "' value, which is " << height << std::endl;
                        std::cout << "Continue reading XML and hope correct value of '"<< CNS_TAG_STY <<"' tag will be encountered later..." << std::endl;
                    }
                    else
                        hasSTY=true;
                }
            }
            //Закончили со Cтарт-Игрек

            //6. Финиш-Икс
            else if(value==CNS_TAG_FINX)
            {
                if (!hasWidth) //Тег "финиш-икс" встретился раньше чем "ширина" - ошибка!
                {
                    std::cout << "Error! '" << CNS_TAG_FINX <<"' tag encountered before '" << CNS_TAG_WIDTH << "' tag." << std::endl;
                    return false;
                }

                if (hasFINX) //Дубль. финиш-икс уже был.
                {
                   std::cout << "Warning! Duplicate '"<< CNS_TAG_FINX <<"' encountered." << std::endl;
                   std::cout << "Only first value of '"<< CNS_TAG_FINX <<"' =" << goal_j <<"will be used." << std::endl;
                }
                else //Финиш-икс еще не встречался (или встречался, но корявый), в общем - надо пытаться инициализировать
                {
                    if (!(stream >> goal_j && goal_j>=0 && goal_j<width))
                    {
                        std::cout << "Warning! Invalid value of '" << CNS_TAG_FINX <<"' tag encountered (or could not convert to integer)" << std::endl;
                        std::cout << "Value of '"<< CNS_TAG_FINX <<"' tag should be an integer AND >=0 AND < '" << CNS_TAG_WIDTH << "' value, which is " << width << std::endl;
                        std::cout << "Continue reading XML and hope correct value of '"<< CNS_TAG_FINX <<"' tag will be encountered later..." << std::endl;
                    }
                    else
                        hasFINX=true;
                }
            }
            //Закончили со Финиш-Икс


            //7. Финиш-Игрек
            else if(value==CNS_TAG_FINY)
            {
                if (!hasHeight) //Тег "финиш-игрек" встретился раньше чем "высота" - ошибка!
                {
                    std::cout << "Error! '" << CNS_TAG_FINY <<"' tag encountered before '" << CNS_TAG_HEIGHT << "' tag." << std::endl;
                    return false;
                }

                if (hasFINY) //Дубль. Финиш-игрек уже был.
                {
                   std::cout << "Warning! Duplicate '"<< CNS_TAG_FINY <<"' encountered." << std::endl;
                   std::cout << "Only first value of '"<< CNS_TAG_FINY <<"' =" << goal_i <<"will be used." << std::endl;
                }
                else //Финиш-Игрек еще не встречался (или встречался, но корявый), в общем - надо пытаться инициализировать
                {
                    if (!(stream >> goal_i && goal_i>=0 && goal_i<height))
                    {
                        std::cout << "Warning! Invalid value of '" << CNS_TAG_FINY <<"' tag encountered (or could not convert to integer)" << std::endl;
                        std::cout << "Value of '"<< CNS_TAG_FINY <<"' tag should be an integer AND >=0 AND < '" << CNS_TAG_HEIGHT << "' value, which is " << height << std::endl;
                        std::cout << "Continue reading XML and hope correct value of '"<< CNS_TAG_FINY <<"' tag will be encountered later..." << std::endl;
                    }
                    else
                        hasFINY=true;
                }
            }

//Checking Grid
            else if(value==CNS_TAG_GRID)
            {
                hasGrid=true;
                if(!(hasHeight && hasWidth))
                {
                    std::cout << "Error! No '" << CNS_TAG_WIDTH <<"' tag or '" << CNS_TAG_HEIGHT << "' tag before '" << CNS_TAG_GRID << "'tag encountered!" << std::endl;
                    return false;
                }
                element=mapTag->FirstChildElement(); //переходим от "грида" к "строкам грида"
                while (grid_i<height)
                {
                    if(!element)
                    {
                        std::cout << "Error! Not enough '" << CNS_TAG_ROW <<"' tags inside '" << CNS_TAG_GRID << "' tag." << std::endl;
                        std::cout << "Number of '" << CNS_TAG_ROW <<"' tags should be equal (or greater) than the value of '" << CNS_TAG_HEIGHT << "' tag which is " << height << std::endl;
                        return false;
                    }
                    row = element -> GetText();
                    rowiter = grid_j = 0;
                    if(row)//проверка на то, что тег row не пустой
                      while (row[rowiter])
                      {
                        if (row[rowiter] != CNS_OTHER_MATRIXSEPARATOR)
                        {
                            if (grid_j == width)
                            {
                                std::cout << "Too many parameters on "<< CNS_TAG_GRID <<" in the " << grid_i + 1 << " "<< CNS_TAG_ROW <<". Continue." << std::endl;
                                break;
                            }
                            stream.str("");
                            stream.clear();
                            stream << &row[rowiter];

                            if (!(stream >> Grid[grid_i][grid_j]))
                            {
                                std::cout << "Invalid value of cell on the grid" << std::endl;
                                return false;
                            }
                            grid_j++;
                        }
                        rowiter++;
                    }

                    if (grid_j != width)
                    {
                        std::cout << "Invalid value on "<< CNS_TAG_GRID <<" in the " << grid_i + 1 << " "<< CNS_TAG_ROW << std::endl;
                        return false;
                    }
                    grid_i++;

                    element=element->NextSiblingElement();
                }
            }
            mapTag=map->IterateChildren(mapTag);
    }

std::cout <<""<< std::endl;

//Checking grid
    if(!hasGrid)
    {
        std::cout<<"Error! There is no tag 'grid' in xml-file!\n";
        return false;
    }

//Checking start & finish
    if(!(hasFINX && hasFINY && hasSTX && hasSTY))   return false;

//Checking start for traverse
    if (Grid[start_i][start_j] == CN_GC_OBS)
    {
        std::cout << "Error! Start cell is not traversable (cell's value is" << CN_GC_OBS << ")!" << std::endl;
        return false;
    }
    else std::cout << "Start cell is traversable." << std::endl;

//Checking finish for traverse
    if (Grid[goal_i][goal_j] == CN_GC_OBS)
    {
        std::cout << "Error! Goal cell is not traversable (cell's value is" << CN_GC_OBS << ")!" << std::endl;
        return false;
    }
    else std::cout << "Finish cell is traversable." << std::endl;

    return true;
}
