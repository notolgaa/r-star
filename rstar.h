#ifndef RSTAR_H
#define RSTAR_H
#include "gl_const.h"
#include "search.h"
#include "astar.h"
#include "logger.h"

class Rstar: public Search
{
public:
    Rstar(float weight, int BT, int SL, int i);
    ~Rstar();

    SearchResult findLocalPath(Node &curNode, NodeList &successorsDist, const EnvironmentOptions &options, NodeList open, const Map &map);

protected:
    //float   computeHFromCellToCell(int start_i, int start_j, int fin_i, int fin_j, const EnvironmentOptions &options);
    //void    addOpen(Node newNode);
    std::list<Node> findSuccessors(Node *curNode, const Map &map, const EnvironmentOptions &options, int distance);
};

#endif // RSTAR_H


