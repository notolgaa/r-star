#include "gl_const.h"
#include "search.h"
#include <iostream>
#ifdef __linux__
    #include <sys/time.h>
#else
    #include <windows.h>
#endif


Search::Search()
{
    //Параметры должны быть корректно переопределены наследниками, в зависимости от того, какая используется "конфигурация" алгоритма
    hweight = 1;
    breakingties = CN_SP_BT_GMAX;
    sizelimit = CN_SP_SL_NOLIMIT;
    open=NULL;
    openSize=0;
    //sresult определять не надо - у него свой конструктор по умолчанию...
}

Search::~Search(void)
{
    if(open)
    {
        delete []open;
    }
}

float Search::MoveCost(int start_i, int start_j, int fin_i, int fin_j, const EnvironmentOptions &options)
{
    if((start_i-fin_i) != 0 && (start_j-fin_j) != 0)
        return options.diagonalcost;
    return options.linecost;
}

bool Search::stopCriterion()
{
    if(openSize==0)
    {
        std::cout << "OPEN list is empty!" << std::endl;
        return true;
    }
    return false;
}

SearchResult Search::startSearch(Logger *log, const Map &map, const EnvironmentOptions &options)
{
    #ifdef __linux__
            timeval begin, end;
            gettimeofday(&begin, NULL);
    #else
        LARGE_INTEGER begin, end, freq;
        QueryPerformanceCounter(&begin);
        QueryPerformanceFrequency(&freq);
    #endif
    open=new NodeList[map.height];
    Node curNode;
    for(int i=0; i<map.height; i++)
        open[i].List.clear();
    curNode.i = map.start_i;
    curNode.j = map.start_j;
    curNode.g = 0;
    curNode.H = computeHFromCellToCell(curNode.i,curNode.j, map.goal_i, map.goal_j,options);
    curNode.F = hweight * curNode.H;
    curNode.parent = 0;

    addOpen(curNode);
    int closeSize=0;
    bool pathfound=false;
    while(!stopCriterion())
    {
        curNode = findMin(map.height);
        close.insert({curNode.i*map.width+curNode.j,curNode});
        closeSize++;
        open[curNode.i].List.pop_front();
        openSize--;
        if(curNode.i==map.goal_i && curNode.j==map.goal_j)
        {

            pathfound=true;
            break;
        }
        std::list<Node> successors=findSuccessors(curNode,map,options);
        std::list<Node>::iterator it=successors.begin();
        while(it!=successors.end())
        {
            it->parent = &(close.find(curNode.i*map.width+curNode.j)->second);
            it->H = computeHFromCellToCell(it->i,it->j,map.goal_i,map.goal_j,options);
            *it=resetParent(*it, *it->parent, map, options);
            it->F = it->g + hweight * it->H;
            addOpen(*it);
            it++;
        }
        log->writeToLogOpenClose(open,close,map.height); //Логгер сам определит писать ему в лог или нет.

    }
    //Поиск завершился!
    sresult.pathfound = false;
    sresult.nodescreated = closeSize + openSize;
    sresult.numberofsteps = closeSize;
    if(pathfound)
    {
        sresult.pathfound = true;
        sresult.pathlength = curNode.g;
        //путь восстанолвенный по обратным указателям (для всех алгоритмов)
        makePrimaryPath(curNode);
    }
    //Т.к. восстановление пути по обратным указателям - неотъемлемая часть алгоритмов, время останавливаем только сейчас!
    #ifdef __linux__
        gettimeofday(&end, NULL);
        sresult.time = (end.tv_sec - begin.tv_sec) + static_cast<double>(end.tv_usec - begin.tv_usec) / 1000000;
    #else
            QueryPerformanceCounter(&end);
            sresult.time = static_cast<double>(end.QuadPart - begin.QuadPart)/freq.QuadPart;
    #endif
    //перестроенный путь (hplevel, либо lplevel,в зависимости от алгоритма)
    if(pathfound)
            makeSecondaryPath(map, curNode);
    return sresult;
}

Node Search::findMin(int size)
{
    Node min;
    min.F=-1;
    for(int i=0; i<size; i++)
    {
        if(open[i].List.size()!=0)
            if(open[i].List.begin()->F<=min.F || min.F==-1)
            {
                if (open[i].List.begin()->F == min.F)
                {
                    switch(breakingties)
                    {
                        case CN_SP_BT_GMAX:
                        {
                            if (open[i].List.begin()->g >= min.g)
                            {
                                min=*open[i].List.begin();
                            }
                        }
                        case CN_SP_BT_GMIN:
                        {
                            if (open[i].List.begin()->g <= min.g)
                            {
                                min=*open[i].List.begin();
                            }
                        }
                    }
                }
                else
                min=*open[i].List.begin();
            }
    }
    return min;

}


std::list<Node> Search::findSuccessors(Node curNode, const Map &map, const EnvironmentOptions &options)
{
    Node newNode;
    std::list<Node> successors;
    for(int i = -1; i <= +1; i++)
    {
        for(int j = -1; j <= +1; j++)
        {
            if((i != 0 || j != 0) && map.CellOnGrid(curNode.i+i,curNode.j+j) && (map.CellIsTraversable(curNode.i+i,curNode.j+j)))
            {
                if(options.allowdiagonal == CN_SP_AD_FALSE)
                    if(i != 0 && j != 0)
                        continue;
                if(options.allowsqueeze == CN_SP_AS_FALSE)
                {
                    if(i != 0 && j != 0)
                        if(map.CellIsObstacle(curNode.i,curNode.j+j) && map.CellIsObstacle(curNode.i+i,curNode.j))
                            continue;
                }
                if(close.find((curNode.i+i)*map.width+curNode.j+j)==close.end())
                {
                    newNode.i = curNode.i+i;
                    newNode.j = curNode.j+j;
                    newNode.g = curNode.g + MoveCost(curNode.i,curNode.j,curNode.i+i,curNode.j+j,options);
                    successors.push_front(newNode);
                }
            }
        }
    }
    return successors;
}

void Search::makePrimaryPath(Node curNode)
{
    Node current=curNode;
    while(current.parent)
    {
        lppath.List.push_front(current);
        current=*current.parent;
    }
    lppath.List.push_front(current);
    sresult.lppath = &lppath; //здесь у sresult - указатель на константу.
}

void Search::makeSecondaryPath(const Map &map, Node curNode)
{
    std::list<Node>::const_iterator iter=lppath.List.begin();
    int curI, curJ, nextI, nextJ, moveI, moveJ;
    hppath.List.push_back(*iter);

    while(iter != --lppath.List.end())
    {
        curI = iter->i;
        curJ = iter->j;
        iter++;
        nextI = iter->i;
        nextJ = iter->j;
        moveI = nextI-curI;
        moveJ = nextJ-curJ;
        iter++;
        if((iter->i - nextI) != moveI || (iter->j - nextJ) != moveJ)
            hppath.List.push_back(*(--iter));
        else
        iter--;
    }
    sresult.hppath = &hppath;
}


void Search::printResultToConsole(Logger *log,const Map &map, SearchResult &sr)
{
    log->writeToLogSummary(sr.numberofsteps, sr.nodescreated, sr.pathlength, sr.time);
    if (sr.pathfound)
    {
        log->writeToLogPath(*sr.lppath);
        log->writeToLogHPpath(*sr.hppath);
        log->writeToLogMap(map,*sr.lppath);
    }
    else
        log->writeToLogNotFound();
    log->saveLog();

    std::cout << "\n\nPath ";
    if (!sr.pathfound)
        std::cout << "NOT ";
    std::cout << "found!" << std::endl;

    std::cout << "\nnumber of steps = " << sr.numberofsteps << std::endl;
    std::cout << "nodes created = " << sr.nodescreated << std::endl;

    if (sr.pathfound)
        std::cout << "path length = " << sr.pathlength << std::endl;
    std::cout << "time = " << sr.time << std::endl;

}
