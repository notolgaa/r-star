QT += core
QT -= gui

TARGET = rsearch
CONFIG += console
CONFIG -= app_bundle

QMAKE_CXXFLAGS += -std=c++11

TEMPLATE = app

SOURCES += main.cpp \
    tinyxml.cpp \
    tinystr.cpp \
    tinyxmlparser.cpp \
    tinyxmlerror.cpp \
    map.cpp \
    environmentoptions.cpp \
    search.cpp \
    list.cpp \
    astar.cpp \
    xmllogger.cpp \
    config.cpp \
    rstar.cpp

HEADERS += \
    tinyxml.h \
    tinystr.h \
    gl_const.h \
    map.h \
    environmentoptions.h \
    node.h \
    search.h \
    searchresult.h \
    list.h \
    logger.h \
    astar.h \
    xmllogger.h \
    config.h \
    rstar.h

