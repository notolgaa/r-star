#ifndef ASTAR_H
#define ASTAR_H
#include "gl_const.h"
#include "search.h"
#include "logger.h"


class Astar : public Search
{
    public:
        Astar(float weight, int BT, int SL, int i);
        ~Astar();

    protected:
        float   computeHFromCellToCell(int start_i, int start_j, int fin_i, int fin_j, const EnvironmentOptions &options);
        void    addOpen(Node newNode);
};


#endif // ASTAR_H
