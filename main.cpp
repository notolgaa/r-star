#include <iostream>
#include "map.h"
#include "astar.h"
#include "rstar.h"
#include "search.h"
#include "environmentoptions.h"
#include "xmllogger.h"
#include "gl_const.h"
#include "config.h"
#include "searchresult.h"

int main()
{
    const char *FileName;
    FileName = "\\4768101.xml"; //Entering the name of XML file
    //FileName = "\\test.xml";

        Map map;
        EnvironmentOptions options;
        Config config;
        XmlLogger* log = new XmlLogger();
        SearchResult srA, srR;

        map.getMap(FileName);
        options.getOptions(FileName);
        config.getConfig(FileName);

        log->loglevel = config.SearchParams[CN_SP_LL];
        log->getLog(FileName, config.LogParams);

        Astar astar(config.SearchParams[CN_SP_HW], config.SearchParams[CN_SP_BT], config.SearchParams[CN_SP_SL], map.height);
        srA = astar.startSearch(log, map, options);

        //Rstar rstar(config.SearchParams[CN_SP_HW], config.SearchParams[CN_SP_BT], config.SearchParams[CN_SP_SL], map.height);
        //srR = rstar.startSearch(log, map, options);


    return 0;
}

